module.exports = {
  default: [
    'features/**/*.feature',
    '--require-module ts-node/register', //typescript cucumber
    '--require ./features/step_definitions/**/*.ts',
  ].join(' ')
}
