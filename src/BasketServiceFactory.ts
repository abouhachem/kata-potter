import { Basket } from './Basket';
import { BasketPriceCalculator } from './BasketPriceCalculator';
import { BasketPriceCalculatorFactoryInterface } from './BasketPriceCalculatorFactory';
import { BasketService, BasketServiceInterface } from './BasketService';
import { BookDiscountPolicy } from './BookDiscountPolicy';

export interface BasketServiceFactoryInterface {
  createBasketService(): BasketServiceInterface;
}

export class BasketServiceFactory implements BasketServiceFactoryInterface {
  constructor(
    private readonly basketPriceCalculatorFactory: BasketPriceCalculatorFactoryInterface
  ) {}

  createBasketService(): BasketServiceInterface {
    // should have a parameter to distinguish between different services (depends on the discount policy)
    return new BasketService(
      new Basket(),
      this.basketPriceCalculatorFactory.createBasketPriceCalculator()
    );
  }
}
