import { BasketPriceCalculator } from './BasketPriceCalculator';
import { BasketPriceCalculatorFactory } from './BasketPriceCalculatorFactory';

describe('BasketPriceCalculatorFactory', () => {
  it('should return a new instance of BasketPriceCalculator', () => {
    // Arrange
    const basketPriceCalculatorFactory = new BasketPriceCalculatorFactory();

    // Act
    const basketPriceCalculator =
      basketPriceCalculatorFactory.createBasketPriceCalculator();

    // Assert
    expect(basketPriceCalculator).toBeInstanceOf(BasketPriceCalculator);
  });
});
