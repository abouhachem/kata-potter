import { Book } from './Book';
import { BookSet, BookSetInterface } from './BookSet';

export interface BasketInterface {
  addBook(book: Book): void;
  getBookSets(): BookSetInterface[];
}

export class Basket implements BasketInterface {
  private readonly bookSets: BookSetInterface[];

  constructor() {
    this.bookSets = [];
  }

  addBook(book: Book) {
    const bookSet = this.bookSets.find((_bookSet) => !_bookSet.contains(book));

    if (bookSet) {
      bookSet.addBook(book);
    } else {
      const newBookSet = new BookSet();
      newBookSet.addBook(book);

      this.bookSets.push(newBookSet);
    }
  }

  getBookSets() {
    return this.bookSets;
  }
}
