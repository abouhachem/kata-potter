import { Basket } from './Basket';
import { Book } from './Book';

describe('Basket', () => {
  let basket: Basket;

  const book1 = new Book('first');
  const book2 = new Book('second');
  const book3 = new Book('third');

  beforeEach(() => {
    basket = new Basket();
  });

  describe('addBook', () => {
    it('should add a book to an existing book set', () => {
      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);

      const bookSets = basket.getBookSets();

      expect(bookSets.length).toBe(1);
      expect(bookSets[0].contains(book1)).toBe(true);
      expect(bookSets[0].contains(book2)).toBe(true);
      expect(bookSets[0].contains(book3)).toBe(true);
    });

    it('should create a new book set for a different book', () => {
      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);

      const book4 = new Book('first');
      basket.addBook(book4);

      const bookSets = basket.getBookSets();

      expect(bookSets.length).toBe(2);
      expect(bookSets[0].contains(book1)).toBe(true);
      expect(bookSets[0].contains(book2)).toBe(true);
      expect(bookSets[0].contains(book3)).toBe(true);
      expect(bookSets[1].contains(book4)).toBe(true);
    });
  });

  describe('getBookSets', () => {
    it('should return an empty array when no books are added', () => {
      const bookSets = basket.getBookSets();

      expect(bookSets.length).toBe(0);
    });

    it('should return the correct book sets', () => {
      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);

      const bookSets = basket.getBookSets();

      expect(bookSets.length).toBe(1);
      expect(bookSets[0].contains(book1)).toBe(true);
      expect(bookSets[0].contains(book2)).toBe(true);
      expect(bookSets[0].contains(book3)).toBe(true);
    });
  });
});
