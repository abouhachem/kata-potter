export type BookTitle = 'first' | 'second' | 'third' | 'fourth' | 'fifth';

export class Book {
  constructor(private readonly title: BookTitle) {}

  equals(book: Book) {
    return this.title === book.title;
  }
}
