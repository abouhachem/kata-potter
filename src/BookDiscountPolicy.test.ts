import { BookDiscountPolicy } from './BookDiscountPolicy';

describe('BookDiscountPolicy', () => {
  describe('getDiscount', () => {
    it('should return 0.95 for a book set of 2', () => {
      // Arrange
      const bookDiscountPolicy = new BookDiscountPolicy();
      const bookSetSize = 2;

      // Act
      const discount = bookDiscountPolicy.getDiscount(bookSetSize);

      // Assert
      expect(discount).toBe(0.95);
    });

    it('should return 0.9 for a book set of 3', () => {
      // Arrange
      const bookDiscountPolicy = new BookDiscountPolicy();
      const bookSetSize = 3;

      // Act
      const discount = bookDiscountPolicy.getDiscount(bookSetSize);

      // Assert
      expect(discount).toBe(0.9);
    });

    it('should return 0.8 for a book set of 4', () => {
      // Arrange
      const bookDiscountPolicy = new BookDiscountPolicy();
      const bookSetSize = 4;

      // Act
      const discount = bookDiscountPolicy.getDiscount(bookSetSize);

      // Assert
      expect(discount).toBe(0.8);
    });

    it('should return 0.75 for a book set of 5', () => {
      // Arrange
      const bookDiscountPolicy = new BookDiscountPolicy();
      const bookSetSize = 5;

      // Act
      const discount = bookDiscountPolicy.getDiscount(bookSetSize);

      // Assert
      expect(discount).toBe(0.75);
    });

    it('should return 1 for a book set of 1', () => {
      // Arrange
      const bookDiscountPolicy = new BookDiscountPolicy();
      const bookSetSize = 1;

      // Act
      const discount = bookDiscountPolicy.getDiscount(bookSetSize);

      // Assert
      expect(discount).toBe(1);
    });

    it('should return 1 for a book set of 6', () => {
      // Arrange
      const bookDiscountPolicy = new BookDiscountPolicy();
      const bookSetSize = 6;

      // Act
      const discount = bookDiscountPolicy.getDiscount(bookSetSize);

      // Assert
      expect(discount).toBe(1);
    });
  });
});
