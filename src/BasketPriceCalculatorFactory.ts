import {
  BasketPriceCalculator,
  BasketPriceCalculatorInterface,
} from './BasketPriceCalculator';
import { BookDiscountPolicy } from './BookDiscountPolicy';

export interface BasketPriceCalculatorFactoryInterface {
  createBasketPriceCalculator(): BasketPriceCalculatorInterface;
}

export class BasketPriceCalculatorFactory
  implements BasketPriceCalculatorFactoryInterface
{
  createBasketPriceCalculator(): BasketPriceCalculatorInterface {
    // should have a parameter to distinguish between different discount policies
    return new BasketPriceCalculator(new BookDiscountPolicy());
  }
}
