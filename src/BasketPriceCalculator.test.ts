import { Basket } from './Basket';
import { Book } from './Book';
import { BasketPriceCalculator } from './BasketPriceCalculator';

describe('BasketPriceCalculator', () => {
  let basketPriceCalculator: BasketPriceCalculator;
  const book1 = new Book('first');
  const book2 = new Book('second');
  const book3 = new Book('third');
  const book4 = new Book('fourth');
  const book5 = new Book('fifth');

  beforeEach(() => {
    basketPriceCalculator = new BasketPriceCalculator();
  });

  describe('calculateBasketPrice', () => {
    it('should calculate the price of an empty basket', () => {
      const basket = new Basket();

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(0);
    });

    it('should calculate the price of a basket with one book', () => {
      const basket = new Basket();

      basket.addBook(book1);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(8);
    });

    it('should calculate the price of a basket with two different books', () => {
      const basket = new Basket();

      basket.addBook(book1);
      basket.addBook(book2);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(15.2);
    });

    it('should calculate the price of a basket with two equal books', () => {
      const basket = new Basket();

      basket.addBook(book1);
      basket.addBook(book1);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(16);
    });

    it('should calculate the price of a basket with three different books', () => {
      const basket = new Basket();

      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(21.6);
    });

    it('should calculate the price of a basket with four different books', () => {
      const basket = new Basket();

      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);
      basket.addBook(book4);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(25.6);
    });

    it('should calculate the price of a basket with five different books', () => {
      const basket = new Basket();

      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);
      basket.addBook(book4);
      basket.addBook(book5);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(30);
    });

    it('should calculate the price of a basket with one book set', () => {
      const basket = new Basket();

      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(21.6);
    });

    it('should optimize book sets: replace 5-3 sets by 4-4 sets', () => {
      const basket = new Basket();

      // set 1: 5 books
      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);
      basket.addBook(book4);
      basket.addBook(book5);

      // set 2: 5 books
      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);
      basket.addBook(book4);
      basket.addBook(book5);

      // set 3: 3 books
      basket.addBook(book1);
      basket.addBook(book2);
      basket.addBook(book3);

      const totalPrice = basketPriceCalculator.calculateBasketPrice(basket);

      expect(totalPrice).toBe(81.2);
    });
  });
});
