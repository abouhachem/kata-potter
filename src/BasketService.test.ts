import { BasketService } from './BasketService';
import { BasketInterface } from './Basket';
import { Book } from './Book';

describe('BasketService', () => {
  let basketService: BasketService;
  let basket: BasketInterface;
  let basketPriceCalculator: { calculateBasketPrice: jest.Mock<number> };

  beforeEach(() => {
    basket = { addBook: jest.fn(), getBookSets: jest.fn() } as BasketInterface;
    basketPriceCalculator = {
      calculateBasketPrice: jest.fn(),
    };
    basketService = new BasketService(basket, basketPriceCalculator);
  });

  describe('addBook', () => {
    it('should add the book into the basket using the default quantity of 1', () => {
      const book = new Book('first');
      basketService.addBook(book);

      expect(basket.addBook).toHaveBeenCalledTimes(1);
      expect(basket.addBook).toHaveBeenCalledWith(book);
    });

    it('should add the book multiple times into the basket', () => {
      const book = new Book('first');
      const quantity = 3;
      basketService.addBook(book, quantity);

      expect(basket.addBook).toHaveBeenCalledTimes(quantity);
      expect(basket.addBook).toHaveBeenCalledWith(book);
    });
  });

  describe('getBasketPrice', () => {
    it('should calculate the basket price using the basket price calculator', () => {
      const expectedPrice = 50;
      basketPriceCalculator.calculateBasketPrice.mockReturnValue(expectedPrice);

      const price = basketService.getBasketPrice();

      expect(price).toBe(expectedPrice);
      expect(basketPriceCalculator.calculateBasketPrice).toHaveBeenCalledWith(
        basket
      );
    });
  });
});
