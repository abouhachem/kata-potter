import { Book } from './Book';

describe('Book', () => {
  const book1 = new Book('first'),
    book2 = new Book('second');

  describe('equals', () => {
    it('should return true when comparing two books with the same title', () => {
      const result = book1.equals(new Book('first'));

      expect(result).toBe(true);
    });

    it('should return false when comparing two books with different titles', () => {
      const result = book1.equals(book2);

      expect(result).toBe(false);
    });
  });
});
