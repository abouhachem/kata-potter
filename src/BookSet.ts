import { Book } from './Book';

export interface BookSetInterface {
  addBook(book: Book): void;
  getBooks(): Book[];
  removeBook(book: Book): void;
  count(): number;
  contains(book: Book): boolean;
}

export class BookSet implements BookSetInterface {
  private readonly books: Book[];

  constructor() {
    this.books = [];
  }

  addBook(book: Book) {
    this.books.push(book);
  }

  getBooks() {
    return [...this.books];
  }

  removeBook(book: Book) {
    const bookIndex = this.books.findIndex((b) => b.equals(book));

    if (bookIndex >= 0) {
      this.books.splice(bookIndex, 1);
    }
  }

  count() {
    return this.books.length;
  }

  contains(book: Book) {
    return this.books.some((b) => b.equals(book));
  }
}
