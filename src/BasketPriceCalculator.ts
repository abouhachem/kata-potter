import { Basket, BasketInterface } from './Basket';
import { BookDiscountPolicyInterface } from './BookDiscountPolicy';
import { BookSetInterface } from './BookSet';

const BOOK_PRICE = 8;

export interface BasketPriceCalculatorInterface {
  calculateBasketPrice(basket: BasketInterface): number;
}

export class BasketPriceCalculator implements BasketPriceCalculatorInterface {
  constructor(
    private readonly bookDiscountPolicy: BookDiscountPolicyInterface
  ) {}

  calculateBasketPrice(basket: Basket): number {
    const bookSets = this.optimizeBookSets(basket.getBookSets());

    const bookSetsPrice = bookSets.reduce((totalPrice, bookSet) => {
      const bookSetPrice = this.calculateBookSetPrice(bookSet);

      return totalPrice + bookSetPrice * 100;
    }, 0);

    return bookSetsPrice / 100;
  }

  private calculateBookSetPrice(bookSet: BookSetInterface) {
    const bookSetSize = bookSet.count();
    const discount = this.getDiscount(bookSetSize);
    const bookSetPrice = bookSetSize * BOOK_PRICE * discount;

    return bookSetPrice;
  }

  private optimizeBookSets(bookSets: BookSetInterface[]): BookSetInterface[] {
    const fiveBookSets = bookSets.filter((bs) => bs.count() === 5);
    const threeBookSets = bookSets.filter((bs) => bs.count() === 3);

    const numberOfSetsToBeOptimized = Math.min(
      fiveBookSets.length,
      threeBookSets.length
    );

    for (let i = 0; i < numberOfSetsToBeOptimized; i++) {
      this.moveOneBookFromFiveSetToThreeSet(fiveBookSets[i], threeBookSets[i]);
    }

    return bookSets;
  }

  private moveOneBookFromFiveSetToThreeSet(
    fiveBookSet: BookSetInterface,
    threeBookSet: BookSetInterface
  ) {
    const bookToMove = fiveBookSet
      .getBooks()
      .find((book) => !threeBookSet.contains(book))!;

    fiveBookSet.removeBook(bookToMove);
    threeBookSet.addBook(bookToMove);
  }

  private getDiscount(bookSetSize: number) {
    switch (bookSetSize) {
      case 2:
        return 0.95;
      case 3:
        return 0.9;
      case 4:
        return 0.8;
      case 5:
        return 0.75;
      default:
        return 1;
    }
  }
}
