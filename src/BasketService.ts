import { BasketInterface } from './Basket';
import { BasketPriceCalculatorInterface } from './BasketPriceCalculator';
import { Book } from './Book';

export interface BasketServiceInterface {
  addBook(book: Book): void;
  getBasketPrice(): number;
}

export class BasketService implements BasketServiceInterface {
  constructor(
    private readonly basket: BasketInterface,
    private readonly basketPriceCalculator: BasketPriceCalculatorInterface
  ) {}

  addBook(book: Book, quantity: number = 1) {
    for (let copy = 0; copy < quantity; copy++) {
      this.basket.addBook(book);
    }
  }

  getBasketPrice() {
    return this.basketPriceCalculator.calculateBasketPrice(this.basket);
  }
}
