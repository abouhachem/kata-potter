export interface BookDiscountPolicyInterface {
  getDiscount(bookSetSize: number): number;
}

export class BookDiscountPolicy implements BookDiscountPolicyInterface {
  getDiscount(bookSetSize: number): number {
    switch (bookSetSize) {
      case 2:
        return 0.95;
      case 3:
        return 0.9;
      case 4:
        return 0.8;
      case 5:
        return 0.75;
      default:
        return 1;
    }
  }
}
