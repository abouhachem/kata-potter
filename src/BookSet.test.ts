import { BookSet } from './BookSet';
import { Book } from './Book';

describe('BookSet', () => {
  let bookSet: BookSet;
  const book1 = new Book('first'),
    book2 = new Book('second'),
    book3 = new Book('third');

  beforeEach(() => {
    bookSet = new BookSet();
  });

  describe('addBook', () => {
    it('should add a book to the book set', () => {
      bookSet.addBook(book1);

      expect(bookSet.count()).toBe(1);
      expect(bookSet.contains(book1)).toBe(true);
    });
  });

  describe('removeBook', () => {
    it('should remove a book from the book set', () => {
      bookSet.addBook(book1);
      bookSet.removeBook(book1);

      expect(bookSet.count()).toBe(0);
      expect(bookSet.contains(book1)).toBe(false);
    });
  });

  describe('count', () => {
    it('should return the number of books in the book set', () => {
      bookSet.addBook(book1);
      bookSet.addBook(book2);

      expect(bookSet.count()).toBe(2);
    });
  });

  describe('contains', () => {
    it('should return true if the book set contains the specified book', () => {
      bookSet.addBook(book1);
      bookSet.addBook(book2);

      expect(bookSet.contains(book1)).toBe(true);
      expect(bookSet.contains(book2)).toBe(true);
    });

    it('should return false if the book set does not contain the specified book', () => {
      bookSet.addBook(book1);
      bookSet.addBook(book2);

      expect(bookSet.contains(book3)).toBe(false);
    });
  });
});
