import { BasketPriceCalculatorFactory } from './BasketPriceCalculatorFactory';
import { BasketService } from './BasketService';
import { BasketServiceFactory } from './BasketServiceFactory';

describe('BasketServiceFactory', () => {
  it('should return a new instance of BasketService', () => {
    // Arrange
    const basketServiceFactory = new BasketServiceFactory(
      new BasketPriceCalculatorFactory() // should be a fake implementation instead
    );

    // Act
    const basketService = basketServiceFactory.createBasketService();

    // Assert
    expect(basketService).toBeInstanceOf(BasketService);
  });
});
