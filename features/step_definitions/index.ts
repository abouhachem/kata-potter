import { Given, When, Then, defineParameterType } from '@cucumber/cucumber';
import assert from 'assert';

import {
  BasketServiceFactory,
  BasketPriceCalculatorFactory,
  Book,
  type BookTitle,
} from '../../src';

defineParameterType({
  name: 'book',
  regexp: /first|second|third|fourth|fifth/,
  transformer(bookTitle: string) {
    return new Book(bookTitle as BookTitle);
  },
});

Given('a new empty basket', function () {
  this.basketService = new BasketServiceFactory(
    new BasketPriceCalculatorFactory()
  ).createBasketService();
});

When(
  'I add {int} copy/copies of {book} book into the basket',
  function (numberOfCopies: number, book: Book) {
    for (let copy = 0; copy < numberOfCopies; copy++) {
      this.basketService.addBook(book);
    }
  }
);

Then(
  'the total price of the basket should be {float}',
  function (float: number) {
    const totalPrice = this.basketService.getBasketPrice();

    assert.equal(totalPrice, float);
  }
);
