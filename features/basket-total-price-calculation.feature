Feature: Discount calculation for Harry Potter books

  Background: 
    Given a new empty basket

  Rule: No discount

    Scenario: No discount for zero books
      Then the total price of the basket should be 0

    Scenario: No discount for one book
      When I add 1 copy of first book into the basket
      Then the total price of the basket should be 8

    Scenario: No discount for two identical books
      When I add 2 copies of first book into the basket
      Then the total price of the basket should be 16

  Rule: Five percent (5%) discount

    Scenario: Five percent (5%) discount for two different books
      When I add 1 copy of first book into the basket
      And I add 1 copy of second book into the basket
      Then the total price of the basket should be 15.2

  Rule: Ten percent (10%) discount

    Scenario: Ten percent (10%) discount for three different books
      When I add 1 copy of first book into the basket
      And I add 1 copy of second book into the basket
      And I add 1 copy of third book into the basket
      Then the total price of the basket should be 21.6

  Rule: Twenty percent (20%) discount

    Scenario: Twenty percent (20%) discount for four different books
      When I add 1 copy of first book into the basket
      And I add 1 copy of second book into the basket
      And I add 1 copy of third book into the basket
      And I add 1 copy of fourth book into the basket
      Then the total price of the basket should be 25.6

  Rule: Twenty-five percent (25%) discount

    Scenario: Twenty-five percent (25%) discount for five different books
      When I add 1 copy of first book into the basket
      And I add 1 copy of second book into the basket
      And I add 1 copy of third book into the basket
      And I add 1 copy of fourth book into the basket
      And I add 1 copy of fifth book into the basket
      Then the total price of the basket should be 30

  Rule: Combination of discounts

    Scenario: A set of five books and a set of one book
      When I add 2 copies of first book into the basket
      And I add 1 copy of second book into the basket
      And I add 1 copy of third book into the basket
      And I add 1 copy of fourth book into the basket
      And I add 1 copy of fifth book into the basket
      Then the total price of the basket should be 38

    Scenario: No discount for the seventh book
      When I add 2 copies of first book into the basket
      And I add 2 copies of second book into the basket
      And I add 1 copy of third book into the basket
      And I add 1 copy of fourth book into the basket
      And I add 1 copy of fifth book into the basket
      Then the total price of the basket should be 45.2

    Scenario: A set of five books and a set of four books and a set of one book
      When I add 3 copies of first book into the basket
      And I add 2 copies of second book into the basket
      And I add 1 copy of third book into the basket
      And I add 1 copy of fourth book into the basket
      And I add 1 copy of fifth book into the basket
      Then the total price of the basket should be 53.2

  Rule: Special case

    Scenario: A set of five books and a set of three books
      When I add 2 copies of first book into the basket
      And I add 2 copies of second book into the basket
      And I add 2 copies of third book into the basket
      And I add 1 copy of fourth book into the basket
      And I add 1 copy of fifth book into the basket
      Then the total price of the basket should be 51.2

    Scenario: Two sets of five books and a set of three books
      When I add 3 copies of first book into the basket
      And I add 3 copies of second book into the basket
      And I add 3 copies of third book into the basket
      And I add 2 copy of fourth book into the basket
      And I add 2 copy of fifth book into the basket
      Then the total price of the basket should be 81.2

    Scenario: Three sets of five books and a set of three books
      When I add 4 copies of first book into the basket
      And I add 4 copies of second book into the basket
      And I add 4 copies of third book into the basket
      And I add 3 copy of fourth book into the basket
      And I add 3 copy of fifth book into the basket
      Then the total price of the basket should be 111.2
