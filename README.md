# Kata Potter

## Steps

- Read the Kata
- Try some use cases in order to understand better.
- Find a strategy
- Start Coding

## Read the Kata

- [Kata Potter Description](./KataPotter.md)

## Try some values

![Play with some values](./.excalidraw.png)

## Find a strategy

As we could see in the examples above, we have to look for sets of 5 books except for one case (5 + 3) which should be (4 + 4) instead.

## Let's code

## Let's write some acceptance tests

In order to validate our code, we need to have some tests.

Let's install `jest` and of course `typescript`. We need babel to compile `jest` with `typescript`.

```
npm i -D typescript jest @types/jest @babel/preset-env @babel/preset-typescript
```

- 0 books

  We create a test for 0 books in the basket with minimum code.

- 1 book

We need a data structure that represents the basket. Let's create a class `Basket`. The basket has books.

- Unit tests vs Functional tests:

I see here we might need unit tests for our basket. And we might have more code to cover in the future by unit tests.

So, it would be better to use `cucumber.js` for functional tests and jest for unit tests.

So let's install `cucumber.js`.

```
npm i -D @cucumber/cucumber ts-node
```

- 2 books

We need to introduce the discount policies here.

We need to know if the 2 books are identical or different.
Let's create a `Book` class.
We need to create sets of books, each set contains different books. And in order to calculate the price of the basket, we need to calculate the price of each set and add them to get the total price of the basket.

Let's encapsulate all this logic in a service (`BasketService`) and handle the other cases except the case of (5 + 3).

For the case of `Three sets of five books and a set of three books`, we have a result of `111.1999999` or we should have `111.2`.

## How to use

In order to use the library for calculating the Basket price, you need to use the `BasketService` which accepts a `Basket` and `BasketPriceCalculator`.

In case you have new price calculation strategy instead of the `BasketPriceCalculation`, you will have to implement the `BasketPriceCalculationInterface`.
